
### Keybase proof

I hereby claim:

  * I am mattronix on github.
  * I am mattronix (https://keybase.io/mattronix) on keybase.
  * I have a public key ASDjZ9vPt3xtkcMsng-FlBeD23fvOMSqswAD9I1nfJXQdQo

To claim this, I am signing this object:

```json
{
    "body": {
        "key": {
            "eldest_kid": "0120e367dbcfb77c6d91c32c9e0f85941783db77ef38c4aab30003f48d677c95d0750a",
            "host": "keybase.io",
            "kid": "0120e367dbcfb77c6d91c32c9e0f85941783db77ef38c4aab30003f48d677c95d0750a",
            "uid": "dc51eda9132dd566f4c8ea2180016a00",
            "username": "mattronix"
        },
        "service": {
            "name": "github",
            "username": "mattronix"
        },
        "type": "web_service_binding",
        "version": 1
    },
    "client": {
        "name": "keybase.io go client",
        "version": "1.0.18"
    },
    "ctime": 1483655255,
    "expire_in": 504576000,
    "merkle_root": {
        "ctime": 1483655224,
        "hash": "20de618fc5bb0ff14a622e46facd2a9c9e0382fd1a4caff929ff266cb79482e43d4c3e7c3b65e2a19b5a9becf70d9daffe4a894dfaca5ee13369134dd5f915cf",
        "seqno": 787629
    },
    "prev": "d686c4e485457deec1f95b828bda7c86bca45d05aa3094255c594ee2579c6418",
    "seqno": 37,
    "tag": "signature"
}
```

with the key [ASDjZ9vPt3xtkcMsng-FlBeD23fvOMSqswAD9I1nfJXQdQo](https://keybase.io/mattronix), yielding the signature:

```
hKRib2R5hqhkZXRhY2hlZMOpaGFzaF90eXBlCqNrZXnEIwEg42fbz7d8bZHDLJ4PhZQXg9t37zjEqrMAA/SNZ3yV0HUKp3BheWxvYWTFAu97ImJvZHkiOnsia2V5Ijp7ImVsZGVzdF9raWQiOiIwMTIwZTM2N2RiY2ZiNzdjNmQ5MWMzMmM5ZTBmODU5NDE3ODNkYjc3ZWYzOGM0YWFiMzAwMDNmNDhkNjc3Yzk1ZDA3NTBhIiwiaG9zdCI6ImtleWJhc2UuaW8iLCJraWQiOiIwMTIwZTM2N2RiY2ZiNzdjNmQ5MWMzMmM5ZTBmODU5NDE3ODNkYjc3ZWYzOGM0YWFiMzAwMDNmNDhkNjc3Yzk1ZDA3NTBhIiwidWlkIjoiZGM1MWVkYTkxMzJkZDU2NmY0YzhlYTIxODAwMTZhMDAiLCJ1c2VybmFtZSI6Im1hdHRyb25peCJ9LCJzZXJ2aWNlIjp7Im5hbWUiOiJnaXRodWIiLCJ1c2VybmFtZSI6Im1hdHRyb25peCJ9LCJ0eXBlIjoid2ViX3NlcnZpY2VfYmluZGluZyIsInZlcnNpb24iOjF9LCJjbGllbnQiOnsibmFtZSI6ImtleWJhc2UuaW8gZ28gY2xpZW50IiwidmVyc2lvbiI6IjEuMC4xOCJ9LCJjdGltZSI6MTQ4MzY1NTI1NSwiZXhwaXJlX2luIjo1MDQ1NzYwMDAsIm1lcmtsZV9yb290Ijp7ImN0aW1lIjoxNDgzNjU1MjI0LCJoYXNoIjoiMjBkZTYxOGZjNWJiMGZmMTRhNjIyZTQ2ZmFjZDJhOWM5ZTAzODJmZDFhNGNhZmY5MjlmZjI2NmNiNzk0ODJlNDNkNGMzZTdjM2I2NWUyYTE5YjVhOWJlY2Y3MGQ5ZGFmZmU0YTg5NGRmYWNhNWVlMTMzNjkxMzRkZDVmOTE1Y2YiLCJzZXFubyI6Nzg3NjI5fSwicHJldiI6ImQ2ODZjNGU0ODU0NTdkZWVjMWY5NWI4MjhiZGE3Yzg2YmNhNDVkMDVhYTMwOTQyNTVjNTk0ZWUyNTc5YzY0MTgiLCJzZXFubyI6MzcsInRhZyI6InNpZ25hdHVyZSJ9o3NpZ8RA2TMCHGpxcBhw/voukJJY/zP7H75jfVMUyE0kir0n+jV1PJUwRIOpJ2quMEjH1M0wjSMIr+58PZDeIpUTAVldCqhzaWdfdHlwZSCkaGFzaIKkdHlwZQildmFsdWXEIEnw7zcAhAQxAOm813oNY0BWestEWpT29DjmV9AFWvXoo3RhZ80CAqd2ZXJzaW9uAQ==

```

And finally, I am proving ownership of the github account by posting this as a gist.

### My publicly-auditable identity:

https://keybase.io/mattronix

### From the command line:

Consider the [keybase command line program](https://keybase.io/download).

```bash
# look me up
keybase id mattronix
```